import java.util.Scanner;

public class Fahrkartenautomat_V2{
	
    public static void main(String[] args)
    {
   	
    Scanner tastatur = new Scanner(System.in);
    	
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       
       do {
    	   //Bestellung erfassen
    	   zuZahlenderBetrag = fahrkartenbestellungErfassen();
    	   
    	   //Eingezahlten Betrag erfassen
    	   eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
             
    	   warte(3450);
    	   }
       
       while(true);
 
    }
    
    
    private static void warte(int milisecond){
    	try {
    		Thread.sleep(milisecond);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    }
        
    private static double[] fahrkartenPreise() {
    	double[] ticketPreise = {0, 2.90,3.30,3.60,1.90,8.60,9.00,9.60,23.50,24.30,24.90};
    	return ticketPreise;
    }
    private static String[] fahrkartenTyp() {
    	String[] ticketTyp	  = {"", "Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC", };
    	return ticketTyp;
    }

    //Gesamtpreis der Tickets
    private static double fahrkartenbestellungErfassen() {
    		Scanner tastatur = new Scanner(System.in); {
			double zuZahlenderBetrag = 0;
			int tickets;
			boolean ticketInputCheck = true;
			int ticketChoice = 0;
			boolean bezahlen = true;
			String[] ticketName = fahrkartenTyp();
			double[] ticketPreise = fahrkartenPreise();
			
			
			do {
			    //Ticketauswahl
				System.out.println("Auswahlnummer  | Preis |          Ticket");
				System.out.println("____________________________________________________________________");
				System.out.println("");
				
				for(int k = 1; k < ticketPreise.length; k++) {
					System.out.printf("%d \t\t %.2f�  \t  %s \n", k, ticketPreise[k], ticketName[k]);
				}
				
				System.out.println("____________________________________________________________________");
				System.out.println("");
				System.out.println("W�hlen Sie Ihr Ticket aus!");
				System.out.println("____________________________________________________________________");
				//System.out.println("Bezahlen");
			
			do { 
				ticketChoice = tastatur.nextInt();
			    	
				if(ticketChoice != ticketPreise.length)
					System.out.println("Ihre Wahl: \n" + ticketChoice + " \t" + ticketName[ticketChoice] + "\n");
					    	
			    if((ticketChoice>=1) && (ticketChoice<=(ticketPreise.length))) 
			    	{ticketInputCheck = false;}
			    
			    else
			    	{System.out.println(" >>falsche Eingabe<<");}		
			    
				}
			while(ticketInputCheck);
				
				
				if(ticketChoice != (ticketPreise.length)) {
			    	ticketInputCheck = true;
			    	
			    	
			        //Anzahl der Tickets
			    	System.out.println("-------------------------------------------------------------");
			        System.out.print("Anzahl der Tickets: ");   	   
			        do {
				        tickets = tastatur.nextInt();
				        
				      //Ung�ltiger Eingabebereich
				        if((tickets>10) || (tickets<1))     	
				        	System.out.println(" >> W�hlen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.\n");
				        else {                
					        zuZahlenderBetrag += ticketPreise[ticketChoice] * tickets;
					        System.out.printf("\nZwischensumme: %.2f �\n\n", zuZahlenderBetrag);				        			        			        	
					        }
				        
				        ticketInputCheck = true;
			        }
			        while(ticketInputCheck);		        
				}	    
				else bezahlen = true;
			}
			while(bezahlen);
			return zuZahlenderBetrag;
		}
    }
    
    private static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        Scanner tastatur = new Scanner(System.in);
        
        double eingeworfeneM�nze;
        double eingezahlterGesamtbetrag = 0.0;
        
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("Noch zu zahlen: " + "%.2f�\n" , (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 0,05�, h�chstens 2,00�): ");
     	   eingeworfeneM�nze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
  
      return eingezahlterGesamtbetrag;
     }
     
     
      private static void fahrkartenAusgeben() {
     	 
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
 		}
        
        System.out.println("\n\n");
      }
      
      private static void r�ckgeldAusgeben(double zuZahlenderBetrag, double eingezahlterGesamtbetrag) {
        // R�ckgeldberechnung und -Ausgabe
        // -------------------------------
        double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(r�ckgabebetrag > 0.0)
        {
     	   System.out.printf("Der R�ckgabebetrag in H�he von " + "%.2f�\n", r�ckgabebetrag );
     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(r�ckgabebetrag >= 2.00f) // 2 EURO-M�nzen
            {
         	  System.out.println("2,00�");
 	          r�ckgabebetrag -= 2.00f;
            }
            while(r�ckgabebetrag >= 1.00f) // 1 EURO-M�nzen
            {
         	  System.out.println("1,00�");
 	          r�ckgabebetrag -= 1.00f;
            }
            while(r�ckgabebetrag >= 0.50f) // 50 CENT-M�nzen
            {
         	  System.out.println("0,50�");
 	          r�ckgabebetrag -= 0.50f;
            }
            while(r�ckgabebetrag >= 0.20f) // 20 CENT-M�nzen
            {
         	  System.out.println("0,20�");
  	          r�ckgabebetrag -= 0.20f;
            }
            while(r�ckgabebetrag >= 0.10f) // 10 CENT-M�nzen
            {
         	  System.out.println("0,10�");
 	          r�ckgabebetrag -= 0.10f;
            }
            while(r�ckgabebetrag >= 0.05f)// 5 CENT-M�nzen
            {
         	  System.out.println("0,05�");
  	          r�ckgabebetrag -= 0.05f;
  	          
  	       
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir w�nschen Ihnen eine gute Fahrt.");
		
	}

}