
public class Notizen {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Ganze Zahl:
		int zahl = 123456789;
		System.out.printf("|%d|%n", zahl); //Pipe schreiben: AltGr und ><
		System.out.printf("|%20d|%n", zahl);
		System.out.printf("|%-20d|%n", zahl);
		
		//Kommazahlen:
		System.out.printf("|%f|%n", 12345.67890);
		System.out.printf("|%.2f|%n", 12345.67890);
		System.out.printf("|%20.2f|%n", 12345.67890);
		System.out.printf("|%-20.2f|%n", 12345.67890);
		
		//Zeichenkette
		System.out.printf("|%s|%n","123456789");
		System.out.printf("|%20s|%n","123456789");
		System.out.printf("|%-20s|%n","123456789");
		System.out.printf("|%s|%n","123456789");
		System.out.printf("|%20.4s|%n","123456789");
		
		//Mehrere Ausgaben
		//"Name:Max,Alter:18,Gehalt:1801.50 Euro"
		System.out.printf("Name: %-10s,Alter: %-10d, Gehalt: %-10.2f Euro %n","Max", 18, 1801.50);
		
		//0!    =                   =   1
	    //5    | |        19       | |  4|
	}

}
