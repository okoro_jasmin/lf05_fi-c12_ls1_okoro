import java.util.Scanner;

public class Schleifen {
	
	public static void main(String[] args) {
		
		Scanner scn = new Scanner(System.in);
		System.out.println("Nummer eingeben: ");
		int nummer = scn.nextInt();
		
		System.out.println("while-Schleife");
		int zaehler = 1;
		while(zaehler <= nummer) {
			System.out.println(zaehler);
			zaehler++;
		
		}
		
		System.out.println("for-Schleife");
		
		for(int i = 1; i <= nummer; i++) {
			System.out.println(i);
		}
		
	}
		
}
