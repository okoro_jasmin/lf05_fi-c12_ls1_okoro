import java.util.Scanner;

public class Fahrkartenautomat_V2{
	
    public static void main(String[] args)
    {
   	
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       
       do {
    	   //Bestellung erfassen
    	   zuZahlenderBetrag = fahrkartenbestellungErfassen();
    	   
    	   //Eingezahlten Betrag erfassen
    	   eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       
    	   //Fahrkarte ausgeben
    	   fahrkartenAusgeben();
    	   //R�ckgeld ausgeben
    	   rueckgeldAusgeben(zuZahlenderBetrag, eingezahlterGesamtbetrag);
       
       }
       while(true);
       
    }
    
        
    private static double[] fahrkartenPreise() {
    	double[] ticketPreise = {0, 2.90,3.30,3.60,1.90,8.60,9.00,9.60,23.50,24.30,24.90};
    	return ticketPreise;
    }
    private static String[] fahrkartenTyp() {
    	String[] ticketTyp	  = {"", "Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC", };
    	return ticketTyp;
    }

    //Gesamtpreis der Tickets
    private static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	double zuZahlenderBetrag = 0;
    	int tickets;
    	boolean ticketInputCheck = true;
    	int ticketChoice = 0;
    	boolean bezahlen = true;
    	String[] ticketName = fahrkartenTyp();
    	double[] ticketPreise = fahrkartenPreise();
    	
    	
    	do {
	        //Ticketauswahl
    		System.out.println("W�hlen Sie Ihr Ticket aus!");
    		System.out.println("Auswahlnummer  | Preis |          Ticket");
    		System.out.println("-------------------------------------------------------------");
    		
    		for(int k = 1; k < ticketPreise.length; k++) {
    			System.out.printf("%d \t\t %.2f�  \t  %s \n", k, ticketPreise[k], ticketName[k]);
    		}
	    
    		System.out.println((ticketPreise.length) + " --Bezahlen--");
    	
    		//G�ltigkeit der Ticketmenge
	    	do { 
	    		ticketChoice = tastatur.nextInt();
		    	
	    		    if(ticketChoice != ticketPreise.length)
	    		    	System.out.println("Ihre Wahl: \n" + ticketChoice + " \t" + ticketName[ticketChoice] + "\n");
	    		    else
	    		    	System.out.println("Bestellvorgang wird beendet!");
	    		    
		    		if((ticketChoice>=1) && (ticketChoice<=(ticketPreise.length))) 
		    			{ticketInputCheck = false;}
		    		else
		    			{System.out.println(" >>falsche Eingabe<<");}		    			
	    	}
	    	while(ticketInputCheck);
	    	
	    	
	    	if(ticketChoice != (ticketPreise.length)) {
		    	ticketInputCheck = true; 
		    	
		    	
		        //Anzahl der Tickets
		        System.out.print("Anzahl der Tickets: ");   	   
		        do {
			        tickets = tastatur.nextInt();
			        
			      //Ung�ltiger Eingabebereich (<1 / >10 Tickets)
			        if((tickets>10) || (tickets<1))     	
			        	System.out.println(" >> W�hlen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.\n");
			        else {                
				        zuZahlenderBetrag += ticketPreise[ticketChoice] * tickets;
				        System.out.printf("\nZwischensumme: %.2f �\n\n", zuZahlenderBetrag);				        			        			        	
				        }
			        
			        ticketInputCheck = false;
		        }
		        while(ticketInputCheck);		        
	    	}	    
	    	else bezahlen = false;
    	}
	    while(bezahlen);
    	return zuZahlenderBetrag;
    }
    
    
    //Eingezahlter Betrag
    private static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	Scanner tastatur = new Scanner(System.in);
    	
        double eingezahlterGesamtbetrag = 0.0;
        double eingeworfeneMuenze = 42.3141726; //Nur f�r die erste Eingabepr�fung
        
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
       
        	if((eingeworfeneMuenze <= 2 && eingeworfeneMuenze >= 0.05) || eingeworfeneMuenze == 42.3141726) {
        		System.out.printf("Noch zu Zahlender Betrag %.2f Euro \n" , (zuZahlenderBetrag - eingezahlterGesamtbetrag));
        		System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
        	}
        	else 
        		System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro)!!!!!: ");
     	   
     	   eingeworfeneMuenze = tastatur.nextDouble();
     	   
     
     	   if(eingeworfeneMuenze <= 2 && eingeworfeneMuenze >= 0.05) 
     		  eingezahlterGesamtbetrag += eingeworfeneMuenze;
     	   
     	   
        }
    return eingezahlterGesamtbetrag;
    }
    
    //Fahrscheinausgabe
    private static void fahrkartenAusgeben() {   
     	   System.out.println("\nFahrschein wird ausgegeben");
     	   
        for (int i = 0; i < 8; i++)
       
        System.out.println("\n");
    	
    }
    
    //R�ckgeld
    private static void rueckgeldAusgeben(double zuZahlenderBetrag, double eingezahlterGesamtbetrag) {
    	
    	int[] coinArray = new int[200];
    	int coinArrayPointer = 0;
        
        double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(r�ckgabebetrag > 0.0)
        {
     	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f", r�ckgabebetrag);
     	   System.out.println(" EURO \nwird in folgenden M�nzen ausgezahlt:");

            while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
            {
            	coinArray[coinArrayPointer] = 200;
            	coinArrayPointer++;
            	r�ckgabebetrag -= 2.0;
            }
            while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
            	coinArray[coinArrayPointer] = 100;
            	coinArrayPointer++;
            	r�ckgabebetrag -= 1.0;
            }
            while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
	             coinArray[coinArrayPointer] = 50;
	           	 coinArrayPointer++;
	 	         r�ckgabebetrag -= 0.5;
            }
            while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
            	coinArray[coinArrayPointer] = 20;
           	  	coinArrayPointer++;
           	  	r�ckgabebetrag -= 0.2;
            }
            while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
            	coinArray[coinArrayPointer] = 10;
            	coinArrayPointer++;
            	r�ckgabebetrag -= 0.1;
            }
            while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
            	coinArray[coinArrayPointer] = 5;
           	 	coinArrayPointer++;
           	 	r�ckgabebetrag -= 0.05;
            }
            while(r�ckgabebetrag >= 0.02)// 2 CENT-M�nzen
            {
            	coinArray[coinArrayPointer] = 2;
           	 	coinArrayPointer++;
           	 	r�ckgabebetrag -= 0.02;
            }
            while(r�ckgabebetrag >= 0.01)// 1 CENT-M�nzen
            {
            	coinArray[coinArrayPointer] = 1;
           	 	coinArrayPointer++;
           	 	r�ckgabebetrag -= 0.01;
            }
            coinArray[coinArrayPointer] = 0;
       	 	coinArrayPointer++;
            muenzeAusgeben(coinArray);
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir w�nschen Ihnen eine gute Fahrt.\n"+
                           "______________________________________\n\n");
    }


	private static void muenzeAusgeben(int[] coinArray) {
		// TODO Auto-generated method stub
		
	}
}