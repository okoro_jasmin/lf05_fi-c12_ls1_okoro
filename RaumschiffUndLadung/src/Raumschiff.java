import java.util.ArrayList;

public class Raumschiff {

	private int photonenTorpedoAnzahl;
	private int energieversorgungInProzent;
	private int huelleInProzent;
	private int schildeInProzent;
	private int lebenserhaltungInProzent;
	private int androidenAnzahl;
	
	private String schiffsname;
	
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	
	public Raumschiff() {
		
	}
	
	public Raumschiff(int photonenTorpedoAnzahl, int energieversorgungInProzent, int huelleInProzent,
			int schildeInProzenz, int lebenserhaltungInProzent, int androidenAnzahl, String schiffsname, 
			ArrayList<Ladung> ladungsverzeichnis, ArrayList<String> broadcastKommunikator) 
	{
		this.photonenTorpedoAnzahl = photonenTorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.huelleInProzent = huelleInProzent;
		this.schildeInProzent = schildeInProzenz;
		this.lebenserhaltungInProzent = lebenserhaltungInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
		this.ladungsverzeichnis = ladungsverzeichnis;
		this.broadcastKommunikator = broadcastKommunikator;
	}

	public int getPhotonenTorpedoAnzahl() {
		return photonenTorpedoAnzahl;
	}

	public void setPhotonenTorpedoAnzahl(int photonenTorpedoAnzahl) {
		this.photonenTorpedoAnzahl = photonenTorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getSchildeInProzenz() {
		return schildeInProzent;
	}

	public void setSchildeInProzenz(int schildeInProzenz) {
		this.schildeInProzent = schildeInProzenz;
	}

	public int getLebenserhaltungInProzent() {
		return lebenserhaltungInProzent;
	}

	public void setLebenserhaltungInProzent(int lebenserhaltungInProzent) {
		this.lebenserhaltungInProzent = lebenserhaltungInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	
	public ArrayList<Ladung> getLadungsverzeichnis() {
        return ladungsverzeichnis;
    }
	
	 public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
	        this.ladungsverzeichnis = ladungsverzeichnis;
	}
	 
	 
	 public void addLadung(Ladung neueLadung) {
	        ladungsverzeichnis.add(neueLadung);
	}
	
	 
	 
	 public void photonentorpedoSchiessen(Raumschiff raumschiff) {
	        treffer(raumschiff);
	        if (photonenTorpedoAnzahl == 0){
	            nachrichtAnAlle("-=*Click*=-");
	            return;
	        } else{
	            setPhotonenTorpedoAnzahl(getPhotonenTorpedoAnzahl() - 1);
	        }
	        nachrichtAnAlle("Photonentorpedo auf " + raumschiff.getSchiffsname() + " abgeschossen");
	    }
	 
	 
	 
	 public void phaserkanoneSchiessen(Raumschiff raumschiff) {
	        if(getEnergieversorgungInProzent() < 50){
	            nachrichtAnAlle("-=*Click*=-");
	        } else {
	            setEnergieversorgungInProzent(getEnergieversorgungInProzent() - 50);
	            nachrichtAnAlle("Phaserkanone auf " + raumschiff.getSchiffsname() + " abgeschossen");
	        }
	        treffer(raumschiff);
	 }

	private void nachrichtAnAlle(String string) {
	}

	private void treffer(Raumschiff raumschiff) {	
	}
	
	public void nachrichtAnAlle(String message) {
        broadcastKommunikator.add(message);
    }

    public ArrayList<String> eintraegeLogbuchZurueckgeben() {
        return new ArrayList<>();
    }

    public void photonentorpedosLaden(int anzahlTorpedos) {
        if (getPhotonenTorpedoAnzahl() == 0){
            System.out.println("Keine Photonentorpedos gefunden!");
            nachrichtAnAlle("-=*Click*=-");
        }
    }
    
    public void zustandRaumschiff() {
        System.out.printf("""
                Zustand f�r Raumschiff= %s
                Photonentorpedo Anzahl= %s
                Energieversorgung in Prozent= %s
                SchildeInProzent= %s
                Huelle in Prozent= %s
                Lebenserhaltungssysteme in Prozent= %s
                Androiden Anzahl= %s
                Schiffsname= %s
                Broadcast Kommunikator= %s
                Ladungsverzeichnis= %s
                """,
                schiffsname, photonenTorpedoAnzahl, energieversorgungInProzent, schildeInProzent,
                huelleInProzent, lebenserhaltungInProzent, androidenAnzahl, schiffsname,
                broadcastKommunikator, ladungsverzeichnis);
    }
    
    public void ladungsverzeichnisAusgeben() {
        System.out.println("Ladungsverzeichnis von Raumschiff: " + getSchiffsname());
        for (Ladung ladung : ladungsverzeichnis){
            System.out.println(ladung.toString());
        }
    }
    
    public String toString() {
        return "Raumschiff - " + schiffsname + '\n' +
                " photonentorpedoAnzahl=" + photonenTorpedoAnzahl +
                ",\n energieversorgungInProzent=" + energieversorgungInProzent +
                ",\n schildeInProzent=" + schildeInProzent +
                ",\n huelleInProzent=" + huelleInProzent +
                ",\n lebenserhaltungssystemeInProzent=" + lebenserhaltungInProzent +
                ",\n androidenAnzahl=" + androidenAnzahl +
                ",\n schiffsname='" + schiffsname + '\'' +
                ",\n broadcastKommunikator=" + broadcastKommunikator +
                ",\n ladungsverzeichnis=" + ladungsverzeichnis;
    	}
	}
